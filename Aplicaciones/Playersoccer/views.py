from django.shortcuts import render, redirect
from .models import Posicion, Equipo, Jugador
from django.contrib import messages

# index
def index(request):
    return render(request, 'index.html')

# Posicion
def listaPosicion(request):
    posicionBdd = Posicion.objects.all()
    return render(request, 'listaPosicion.html', {'posiciones': posicionBdd})

def eliminarPosicion(request,id_pos):
    posicionEliminar=Posicion.objects.get(id_pos=id_pos)
    posicionEliminar.delete()
    messages.warning(request,'Posicion eliminado correctamente')
    return redirect('/listaPosicion')

def guardarPosicion(request):
    nombre_pos=request.POST["nombre_pos"]
    descripcion_pos=request.POST["descripcion_pos"]
    nuevoPosicion=Posicion.objects.create(nombre_pos=nombre_pos,
    descripcion_pos=descripcion_pos)
    messages.success(request,'Posicion guardado correctamente')
    return redirect('listaPosicion')

def editarPosicion(request,id_pos):
    posicionEditar=Posicion.objects.get(id_pos=id_pos)
    return render(request, 'editarPosicion.html',{'posicion':posicionEditar})

def procesarActualizacionPosicion(request):
    id_pos=request.POST["id_pos"]
    nombre_pos=request.POST["nombre_pos"]
    descripcion_pos=request.POST["descripcion_pos"]

    posicionEditar=Posicion.objects.get(id_pos=id_pos)
    posicionEditar.nombre_pos=nombre_pos
    posicionEditar.descripcion_pos=descripcion_pos
    posicionEditar.save()
    messages.success(request,'Posicion actualizado correctamente')
    return redirect('listaPosicion')    


# Equipo
def listaEquipo(request):
    equipoBdd = Equipo.objects.all()
    return render(request, 'listaEquipo.html', {'equipos': equipoBdd})

def eliminarEquipo(request,id_equi):
    equipoEliminar=Equipo.objects.get(id_equi=id_equi)
    equipoEliminar.delete()
    messages.warning(request,'Equipo eliminado correctamente')
    return redirect('/listaEquipo')

def guardarEquipo(request):
    nombre_equi=request.POST["nombre_equi"]
    siglas_equi=request.POST["siglas_equi"]
    fundacion_equi=request.POST["fundacion_equi"]
    region_equi=request.POST["region_equi"]
    numero_titulos_equi=request.POST["numero_titulos_equi"]

    nuevoEquipo=Equipo.objects.create(nombre_equi=nombre_equi,
    siglas_equi=siglas_equi, fundacion_equi=fundacion_equi,
    region_equi=region_equi, numero_titulos_equi=numero_titulos_equi)
    messages.success(request,'Equipo guardado correctamente')
    return redirect('listaEquipo')

def editarEquipo(request,id_equi):
    equipoEditar=Equipo.objects.get(id_equi=id_equi)
    return render(request, 'editarEquipo.html',{'equipo':equipoEditar})

def procesarActualizacionEquipo(request):
    id_equi=request.POST["id_equi"]
    nombre_equi=request.POST["nombre_equi"]
    siglas_equi=request.POST["siglas_equi"]
    fundacion_equi=request.POST["fundacion_equi"]
    region_equi=request.POST["region_equi"]
    numero_titulos_equi=request.POST["numero_titulos_equi"]

    equipoEditar=Equipo.objects.get(id_equi=id_equi)
    equipoEditar.nombre_equi=nombre_equi
    equipoEditar.siglas_equi=siglas_equi
    equipoEditar.fundacion_equi=fundacion_equi
    equipoEditar.region_equi=region_equi
    equipoEditar.numero_titulos_equi=numero_titulos_equi
    equipoEditar.save()
    messages.success(request,'Equipo actualizado correctamente')
    return redirect('listaEquipo')    

# JUGADOR

def listaJugador(request):
    jugadorBdd = Jugador.objects.all()
    posicionBdd = Posicion.objects.all()
    equipoBdd = Equipo.objects.all()
    return render(request, 'listaJugador.html', {
        'jugadores': jugadorBdd,
        'posiciones': posicionBdd,
        'equipos': equipoBdd
        })

def eliminarJugador(request,id_jug):
    jugadorEliminar=Jugador.objects.get(id_jug=id_jug)
    jugadorEliminar.delete()
    messages.warning(request,'Jugador eliminado correctamente')
    return redirect('/listaJugador')

def guardarJugador(request):
    id_pos=request.POST["id_pos"]
    posicionSeleccionado=Posicion.objects.get(id_pos=id_pos)
    
    id_equi=request.POST["id_equi"]
    equipoSeleccionado=Equipo.objects.get(id_equi=id_equi)
    

    apellido_jug=request.POST["apellido_jug"]
    nombre_jug=request.POST["nombre_jug"]
    estatura_jug=request.POST["estatura_jug"]
    salario_jug=request.POST["salario_jug"]
    estado_jug=request.POST["estado_jug"]

    nuevoJugador=Jugador.objects.create(
        posicion=posicionSeleccionado,
        equipo=equipoSeleccionado,
        apellido_jug=apellido_jug,
        nombre_jug=nombre_jug, 
        estatura_jug=estatura_jug,
        salario_jug=salario_jug, 
        estado_jug=estado_jug)
    messages.success(request,'Jugador guardado correctamente')
    return redirect('listaJugador')

def editarJugador(request,id_jug):
    jugadorEditar=Jugador.objects.get(id_jug=id_jug)
    posicionBdd = Posicion.objects.all()
    equipoBdd = Equipo.objects.all()
    return render(request, 'editarJugador.html',{
        'jugador':jugadorEditar,
        'posiciones': posicionBdd,
        'equipos': equipoBdd
        })

def procesarActualizacionJugador(request):
    id_pos=request.POST["id_pos"]
    posicionSeleccionado=Posicion.objects.get(id_pos=id_pos)
    
    id_equi=request.POST["id_equi"]
    equipoSeleccionado=Equipo.objects.get(id_equi=id_equi)
    
    id_jug=request.POST["id_jug"]
    apellido_jug=request.POST["apellido_jug"]
    nombre_jug=request.POST["nombre_jug"]
    estatura_jug=request.POST["estatura_jug"]
    salario_jug=request.POST["salario_jug"]
    estado_jug=request.POST["estado_jug"]

    jugadorEditar=Jugador.objects.get(id_jug=id_jug)
    jugadorEditar.posicion=posicionSeleccionado
    jugadorEditar.equipo=equipoSeleccionado
    jugadorEditar.apellido_jug=apellido_jug
    jugadorEditar.nombre_jug=nombre_jug
    jugadorEditar.estatura_jug=estatura_jug
    jugadorEditar.salario_jug=salario_jug 
    jugadorEditar.estado_jug=estado_jug
    jugadorEditar.save()
    messages.success(request,'Jugador actualizado correctamente')
    return redirect('listaJugador')    
