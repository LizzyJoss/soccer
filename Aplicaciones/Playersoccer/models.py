from django.db import models

# Create your models here.
class Posicion(models.Model):
    id_pos = models.AutoField(primary_key=True)
    nombre_pos = models.CharField(max_length=150)
    descripcion_pos = models.TextField()
    class Meta:
        managed = False  
        db_table = 'posicion'

class Equipo(models.Model):
    id_equi = models.AutoField(primary_key=True)
    nombre_equi = models.CharField(max_length=500)
    siglas_equi = models.CharField(max_length=25)
    fundacion_equi = models.CharField(max_length=150)
    region_equi = models.CharField(max_length=25)
    numero_titulos_equi = models.CharField(max_length=150)
    class Meta:
        managed = False  
        db_table = 'equipo'

class Jugador(models.Model):
    id_jug = models.AutoField(primary_key=True)
    apellido_jug = models.CharField(max_length=500)
    nombre_jug = models.CharField(max_length=500)
    estatura_jug = models.CharField(max_length=500)
    salario_jug = models.CharField(max_length=500)
    estado_jug = models.CharField(max_length=500)
    posicion = models.ForeignKey(Posicion, null=True, blank=True, on_delete=models.PROTECT, db_column='fk_id_pos')
    equipo  = models.ForeignKey(Equipo, null=True, blank=True, on_delete=models.PROTECT, db_column='fk_id_equi')
    class Meta:
        managed = False  
        db_table = 'jugador'