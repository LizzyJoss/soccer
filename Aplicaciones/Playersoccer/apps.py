from django.apps import AppConfig


class PlayersoccerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.Playersoccer'
