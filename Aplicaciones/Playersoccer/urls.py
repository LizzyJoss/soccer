from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('listaPosicion/', views.listaPosicion, name='listaPosicion'),
    path('eliminarPosicion/<id_pos>', views.eliminarPosicion, name='eliminarPosicion'),
    path('guardarPosicion/', views.guardarPosicion),
    path('editarPosicion/<id_pos>', views.editarPosicion),
    path('procesarActualizacionPosicion/', views.procesarActualizacionPosicion),
    path('listaEquipo/', views.listaEquipo, name='listaEquipo'),
    path('eliminarEquipo/<id_equi>', views.eliminarEquipo, name='eliminarEquipo'),
    path('guardarEquipo/', views.guardarEquipo),
    path('editarEquipo/<id_equi>', views.editarEquipo),
    path('procesarActualizacionEquipo/', views.procesarActualizacionEquipo),
    path('listaJugador/', views.listaJugador, name='listaJugador'),
    path('eliminarJugador/<id_jug>', views.eliminarJugador, name='eliminarJugador'),
    path('guardarJugador/', views.guardarJugador),
    path('editarJugador/<id_jug>', views.editarJugador),
    path('procesarActualizacionJugador/', views.procesarActualizacionJugador)

]